﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/ConnectionString")]
public class ConnectionStringDefinition : ScriptableObject
{
    public string ConnectionString;
}
