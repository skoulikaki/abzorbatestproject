﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/RestPaths")]
public class RestServicePathsDefinition : ScriptableObject
{
    public string GetSpinnerValuesPath;
    public string PostMakeSpinPath;
}
