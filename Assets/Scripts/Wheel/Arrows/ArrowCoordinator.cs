﻿using UnityEngine;

public class ArrowCoordinator : MonoBehaviour
{
    public float delayBeforeBrake = .5f;
    public float degreesOfMovement = 5;
    [Range(.01f, 1f)]
    public float guaranteedIntensity = .1f;

    public ArrowSpinsFrantically leftArrow;
    public ArrowSpinsFrantically rightArrow;

    private bool upOrDownFlip;

    [HideInInspector]
    public float intensity01;

    private ArrowState arrowState = ArrowState.Idle;
    private ArrowParticleIntensity leftArrowParticleIntensity;
    private ArrowParticleIntensity rightArrowParticleIntensity;

    private enum ArrowState { Idle, Spinning, Braking }

    private void Awake()
    {
        leftArrowParticleIntensity = leftArrow.GetComponentInChildren<ArrowParticleIntensity>();
        rightArrowParticleIntensity = rightArrow.GetComponentInChildren<ArrowParticleIntensity>();
    }

    void Update()
    {
        if (arrowState != ArrowState.Idle)
            SpinFrantically();
    }

    private void SpinFrantically()
    {
        SpinWithRandomAngle();
        ApplyBrakeIfNeeded();
        SetParticleIntensity();
    }

    private void SpinWithRandomAngle()
    {
        var randomRange = UnityEngine.Random.value * degreesOfMovement * (upOrDownFlip ? 1 : -1) * (intensity01 + guaranteedIntensity);
        leftArrow.SpinWithAngle(randomRange);
        rightArrow.SpinWithAngle(-randomRange);
    }

    private void ApplyBrakeIfNeeded()
    {
        if (arrowState == ArrowState.Braking)
        {
            upOrDownFlip = true;
            if (leftArrow.IsAngleReset || rightArrow.IsAngleReset)
            {
                arrowState = ArrowState.Idle;
                intensity01 = 0;
            }
        }
        else
            upOrDownFlip = !upOrDownFlip;
    }

    public void StartArrowSpinning()
    {
        arrowState = ArrowState.Spinning;
    }

    public void StopArrowSpinning()
    {
        Invoke(nameof(BrakeArrowDelayed), delayBeforeBrake);
    }

    void BrakeArrowDelayed()
    {
        arrowState = ArrowState.Braking;
    }

    private void SetParticleIntensity()
    {
        rightArrowParticleIntensity.Intensity01 = leftArrowParticleIntensity.Intensity01 = intensity01;
    }
}
