﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowSpinsFrantically : MonoBehaviour
{
    public float limitMin = -5;
    public float limitMax = 5;

    private float currentAngle;
    private float startingAngle;

    public bool IsAngleReset { get { return currentAngle == startingAngle; } }

    private void Start()
    {
        currentAngle = startingAngle = transform.localEulerAngles.z;
    }

    internal void SpinWithAngle(float angle)
    {
        currentAngle += angle;
        currentAngle = Mathf.Clamp(currentAngle, limitMin, limitMax);
        transform.rotation = Quaternion.Euler(0.0f, 0.0f, currentAngle);
    }
}
