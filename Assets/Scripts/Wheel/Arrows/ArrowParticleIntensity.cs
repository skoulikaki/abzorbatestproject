﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowParticleIntensity : MonoBehaviour
{
    private ParticleSystem.EmissionModule emissionSystem;

    public int maxRateOverTime = 500;

    private void Awake()
    {
        emissionSystem = GetComponentInChildren<ParticleSystem>().emission;
    }

    public float Intensity01
    {
        set
        {
            emissionSystem.rateOverTime = maxRateOverTime * value;
        }
    }
}
