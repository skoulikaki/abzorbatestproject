﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinnerMotor : MonoBehaviour
{
    public ArrowCoordinator arrowCoordinator;

    public float firstSpinPositionY = 2f;
    public float spinValuesDistance = 2f;

    public float accelerationRate = 1f;
    public float decelerationRate = 1f;
    public float brakeDistance = 1f;

    private float currentSpinSpeed;

    [HideInInspector]
    public float spinSpeed = 1f;

    private RectTransform evenDistributorRectTransform;
    private float maxYPosition;
    private float yPositionBeforeReposition;
    private float yToStopAt;

    private SpinState spinState;
    private float trueDistanceAtTimeOfBraking;
    private Action onWheelIdleCallback;
    private const float practicallyZero = .03f;

    enum SpinState
    {
        Idle, Accelerating, Spinning, AwaitBraking, ReadyToBreak, Braking
    }

    private void Awake()
    {
        evenDistributorRectTransform = GetComponent<RectTransform>();
    }

    private void Update()
    {
        if (spinState != SpinState.Idle)
        {
            CalculateSpeedOfWheel();
            SpinWheel();
            RepositionWholeWheelIfNeeded();
            RepositionSpinValuesForInfiniteScroll();
        }
    }

    public void SetInitialPositions()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).localPosition = new Vector2(0, -i * spinValuesDistance);
        }

        transform.localPosition = new Vector2(transform.localPosition.x, firstSpinPositionY);

        maxYPosition = transform.childCount * spinValuesDistance;
        yPositionBeforeReposition = maxYPosition / 2;
    }

    public void StartWheelSpinning()
    {
        spinState = SpinState.Accelerating;
    }

    public void StopWheelSpinningAtIndex(int resultIndex, Action onWheelIdle)
    {
        if (currentSpinSpeed > 0)
            yToStopAt = spinValuesDistance * resultIndex;
        else
            yToStopAt = -(spinValuesDistance * (transform.childCount - resultIndex));
        spinState = SpinState.AwaitBraking;
        onWheelIdleCallback = onWheelIdle;
    }

    private void CalculateSpeedOfWheel()
    {
        float trueDistanceFromStop = TrueDistance(yToStopAt, transform.localPosition.y);
        if (spinState == SpinState.AwaitBraking)
        {
            if (trueDistanceFromStop >= brakeDistance)
                spinState = SpinState.ReadyToBreak;
        }
        if (spinState == SpinState.ReadyToBreak)
        {
            if (trueDistanceFromStop <= brakeDistance)
            {
                spinState = SpinState.Braking;
                trueDistanceAtTimeOfBraking = trueDistanceFromStop;
            }
        }

        if (spinState == SpinState.Braking)
        {
            currentSpinSpeed = Mathf.Min(trueDistanceFromStop * decelerationRate, Mathf.Abs(currentSpinSpeed)) * Mathf.Sign(currentSpinSpeed);

            if (trueDistanceFromStop < practicallyZero || trueDistanceFromStop > trueDistanceAtTimeOfBraking)
            {
                transform.Translate(0, trueDistanceFromStop * Mathf.Sign(currentSpinSpeed), 0, Space.Self);
                arrowCoordinator.intensity01 = 0;
                currentSpinSpeed = 0;
                spinState = SpinState.Idle;
                onWheelIdleCallback?.Invoke();
                Debug.Log("Wheel stopped");
            }
        }
        else if (spinState == SpinState.Accelerating)
        {
            currentSpinSpeed = Mathf.Lerp(currentSpinSpeed, spinSpeed, accelerationRate * Time.deltaTime);
            if (Mathf.Abs(currentSpinSpeed - spinSpeed) < practicallyZero)
            {
                currentSpinSpeed = spinSpeed;
                spinState = SpinState.Spinning;
            }
        }
    }

    private void SpinWheel()
    {
        arrowCoordinator.intensity01 = currentSpinSpeed / spinSpeed;
        transform.Translate(0, currentSpinSpeed * Time.deltaTime, 0, Space.Self);
    }

    private void RepositionWholeWheelIfNeeded()
    {
        if ((evenDistributorRectTransform.localPosition.y > maxYPosition) || (evenDistributorRectTransform.localPosition.y < -maxYPosition))
            evenDistributorRectTransform.localPosition = new Vector2(evenDistributorRectTransform.localPosition.x, evenDistributorRectTransform.localPosition.y % maxYPosition - maxYPosition * Math.Sign(currentSpinSpeed));
    }

    private void RepositionSpinValuesForInfiniteScroll()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform currentChild = transform.GetChild(i);
            float yDiff = 0;
            float childPosition = currentChild.localPosition.y + evenDistributorRectTransform.localPosition.y;

            if (childPosition > yPositionBeforeReposition)
            {
                yDiff = -maxYPosition * (int)((yPositionBeforeReposition + childPosition) / maxYPosition);
            }
            else if (childPosition < -yPositionBeforeReposition)
            {
                yDiff = maxYPosition * (int)((yPositionBeforeReposition - childPosition) / maxYPosition);
            }
            currentChild.localPosition = new Vector2(currentChild.localPosition.x, currentChild.localPosition.y + yDiff);
        }
    }

    private float TrueDistance(float stopPoint, float y)
    {
        float distance = Mathf.Abs(y - stopPoint);
        if (currentSpinSpeed < 0)
        {
            if (y < stopPoint)
                distance -= maxYPosition;
        }
        else
        {
            if (y > stopPoint)
                distance += maxYPosition;
        }

        return distance;
    }
}
