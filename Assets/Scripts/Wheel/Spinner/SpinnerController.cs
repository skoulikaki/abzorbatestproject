﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SpinnerController : MonoBehaviour
{
    public SpinnerMotor spinValueContainer;
    public RectTransform spinValuePrefab;

    private long[] cachesValues;

    public void SetDisplaySpinnerValues(long[] spinnerValues)
    {
        foreach (var eachValueToInstantiate in spinnerValues)
        {
            var spinValueInstance = Instantiate(spinValuePrefab, spinValueContainer.transform);
            spinValueInstance.GetComponent<TextMeshProUGUI>().text = eachValueToInstantiate.ToString();
        }

        spinValueContainer.SetInitialPositions();
        cachesValues = spinnerValues;
    }

    public void StopWheelSpinningAtResult(long result, Action onWheelIdle)
    {
        // We assume that service will always return a valid value
        var indexToStopOn = Array.IndexOf(cachesValues, result);
        spinValueContainer.StopWheelSpinningAtIndex(indexToStopOn, onWheelIdle);
    }

    internal void SetWheelSpeed(float spinSpeed)
    {
        spinValueContainer.spinSpeed = spinSpeed;
    }

    internal void StartWheelSpinning()
    {
        spinValueContainer.StartWheelSpinning();
    }
}
