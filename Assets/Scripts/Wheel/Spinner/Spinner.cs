﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour
{
    public Animator greyingOutArea;
    public Animator tapToSpin;
    public Animator cameraAnimator;
    public AnimationEndWinningSum winningSum;

    public float spinSpeed = -10f;
    public float delayForWheelSpin = .5f;
    public float delayWheelGuaranteed = 3;

    public event Action<Action<long>> RequestSpinResult;

    private ArrowCoordinator arrowCoordinator;
    private SpinnerController spinnerController;

    private bool wheelIsActive;

    const string FadeOutTrigger = "FadeOut";
    const string FadeInTrigger = "FadeIn";
    const string ZoomInTrigger = "ZoomIn";
    const string ZoomOutTrigger = "ZoomOut";

    private void Awake()
    {
        arrowCoordinator = GetComponentInChildren<ArrowCoordinator>();
        spinnerController = GetComponentInChildren<SpinnerController>();
    }

    private void Start()
    {
        spinnerController.SetWheelSpeed(spinSpeed);
    }

    public void SetValues(SpinnerValuesDao spinnerValuesDao)
    {
        spinnerController.SetDisplaySpinnerValues(spinnerValuesDao.spinnerValues);
    }

    public void TapToSpinClicked()
    {
        if (!wheelIsActive)
            StartCoroutine(nameof(FullWhellSpin));
        Debug.Log("TapToSpinClicked");
    }

    void CallBackWithIndexResult(long result)
    {
        arrowCoordinator.StopArrowSpinning();
        spinnerController.StopWheelSpinningAtResult(result, () => StartCoroutine(nameof(WheelHasStopped), result));
    }

    IEnumerator FullWhellSpin()
    {
        yield return StartCoroutine(nameof(SpinTheWheel));
        yield return new WaitForSeconds(delayWheelGuaranteed);
        RequestSpinResult(CallBackWithIndexResult);
    }

    IEnumerator SpinTheWheel()
    {
        wheelIsActive = true;
        greyingOutArea.SetTrigger(FadeOutTrigger);
        tapToSpin.SetTrigger(FadeOutTrigger);
        cameraAnimator.SetTrigger(ZoomInTrigger);
        yield return new WaitForSeconds(delayForWheelSpin);
        spinnerController.StartWheelSpinning();
        arrowCoordinator.StartArrowSpinning();
    }

    IEnumerator WheelHasStopped(long result)
    {
        yield return new WaitForSeconds(delayForWheelSpin);
        greyingOutArea.SetTrigger(FadeInTrigger);
        cameraAnimator.SetTrigger(ZoomOutTrigger);
        winningSum.FadeIn(result,() =>
        {
            tapToSpin.SetTrigger(FadeInTrigger);
            wheelIsActive = false;
        });
    }
}
