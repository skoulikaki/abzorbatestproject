﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class FpsCounter : MonoBehaviour
{
    int frames;
    float deltaTime = 0.0f;

    private Text fpsText;
    private float fps;

    private void Awake()
    {
        fpsText = GetComponent<Text>();
    }

    void Update()
    {
        deltaTime += Time.deltaTime;
        frames++;
        if (deltaTime > 1f)
        {
            fps = frames;
            deltaTime %= 1f;
            frames = 0;
        }
        fpsText.text = string.Format("{0:0.0} ms ({1:0.} fps)", Time.deltaTime * 1000, fps);
    }
}
