﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SingleSpinnerValueDao
{
    // NB : Field names here are used in rest calls. Be extra careful
    public long spinnerValue;
}
