﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AnimationEndWinningSum : MonoBehaviour
{
    const string FadeInTrigger = "FadeIn";

    private Animator textAnimator;
    private TextMeshProUGUI textMesh;
    private Action onAnimationComplete;

    private void Awake()
    {
        textAnimator = GetComponent<Animator>();
        textMesh = GetComponent<TextMeshProUGUI>();
    }

    public void OnAnimationEndWinningSum()
    {
        Debug.Log("OnAnimationEndWinningSum");
        onAnimationComplete?.Invoke();
    }

    internal void FadeIn(long result, Action onAnimationCompleteCallback)
    {
        textMesh.text = result.ToString("N0");
        onAnimationComplete = onAnimationCompleteCallback;
        textAnimator.SetTrigger(FadeInTrigger);
    }
}
