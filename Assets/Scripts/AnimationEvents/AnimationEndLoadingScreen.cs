﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Image))]
public class AnimationEndLoadingScreen : MonoBehaviour
{
    private Animator loadingScreenAnimator;
    private Image loadingScreenImage;

    private void Awake()
    {
        loadingScreenAnimator = GetComponent<Animator>();
        loadingScreenImage = GetComponent<Image>();
    }

    public void OnLoadingScreenIsHidden()
    {
        loadingScreenAnimator.enabled = false;
        loadingScreenImage.enabled = false;
    }
}
