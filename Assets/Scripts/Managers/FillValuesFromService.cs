﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RestApiCalls))]
public class FillValuesFromService : MonoBehaviour
{
    const string FadeInTrigger = "FadeIn";

    public Animator loadingScreenAnimator;

    public Spinner[] spinnersToProvideValuesFor;
    private RestApiCalls restApiCalls;

    private void Awake()
    {
        restApiCalls = GetComponent<RestApiCalls>();
    }

    void Start()
    {
        restApiCalls.GetSpinnerValuesFromRest(ValuesFromServiceSuccess, ServiceError);
    }

    private void ValuesFromServiceSuccess(SpinnerValuesDao spinnerValuesDao, float latencyMilliseconds)
    {
        Debug.LogFormat("Service reply values msecs : {0}", latencyMilliseconds);
        foreach (var eachSpinner in spinnersToProvideValuesFor)
        {
            eachSpinner.SetValues(spinnerValuesDao);
            eachSpinner.RequestSpinResult += EachSpinner_RequestSpinResult;
        }

        loadingScreenAnimator.SetTrigger(FadeInTrigger);
    }

    private void EachSpinner_RequestSpinResult(Action<long> callbackWithSpinnerValue)
    {
        restApiCalls.PostSingleSpinnerValueFromRest((singleSpinnerValueDao, latencyMilliseconds) =>
        {
            ResultValueFromServiceSuccess(callbackWithSpinnerValue, singleSpinnerValueDao, latencyMilliseconds);
        }, ServiceError);
    }

    private void ResultValueFromServiceSuccess(Action<long> callbackWithSpinnerValue, SingleSpinnerValueDao singleSpinnerValueDao, float latencyMilliseconds)
    {
        Debug.LogFormat("Service reply result msecs : {0}", latencyMilliseconds);
        callbackWithSpinnerValue(singleSpinnerValueDao.spinnerValue);
    }

    private void ServiceError(string errorDescription)
    {
        Debug.LogErrorFormat("Service Error : {0}", errorDescription);
    }
}
