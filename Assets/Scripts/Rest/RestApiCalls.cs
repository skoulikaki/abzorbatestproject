﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class RestApiCalls : MonoBehaviour
{
    public ConnectionStringDefinition connectionStringStaging;
    public ConnectionStringDefinition connectionStringProduction;
    public RestServicePathsDefinition restPaths;
    public int timeoutSeconds = 10;

    public NetworkEnvironment networkEnvironment = NetworkEnvironment.Staging;

    public enum NetworkEnvironment
    {
        Staging, Production
    };

    public void PostSingleSpinnerValueFromRest(Action<SingleSpinnerValueDao, float> successAction, Action<string> failAction)
    {
        StartCoroutine(SendUnityWebRequest(
            (uri) => UnityWebRequest.Post(uri, string.Empty), // Body of the request is empty
            restPaths.PostMakeSpinPath,
            (webRequest, responseTime) => ProcessServiceResponse<SingleSpinnerValueDao>(webRequest, responseTime, successAction, failAction),
            failAction));
    }

    public void GetSpinnerValuesFromRest(Action<SpinnerValuesDao, float> successAction, Action<string> failAction)
    {
        StartCoroutine(SendUnityWebRequest(
            (uri) => UnityWebRequest.Get(uri),
            restPaths.GetSpinnerValuesPath,
            (webRequest, responseTime) => ProcessServiceResponse<SpinnerValuesDao>(webRequest, responseTime, successAction, failAction),
            failAction));
    }

    void ProcessServiceResponse<T>(UnityWebRequest webRequest, float responseTime, Action<T, float> successAction, Action<string> failAction)
    {
        string jsonResponse = webRequest.downloadHandler.text;
        var spinnerValuesDao = JsonUtility.FromJson<T>(jsonResponse);
        if (spinnerValuesDao != null)
        {
            successAction(spinnerValuesDao, responseTime * 1000); // Convert to ms
            Debug.Log(webRequest.downloadHandler.text);
        }
        else
        {
            failAction("PostSingleSpinnerValueFromRest result was NOT a " + typeof(T).ToString());
            Debug.LogWarning(webRequest.error);
        }
    }

    IEnumerator SendUnityWebRequest(Func<Uri, UnityWebRequest> generateWebRequest, string apiPath, Action<UnityWebRequest, float> handleSucessResult, Action<string> failAction)
    {
        var connectionEnv = IsStagingEnv
            ? connectionStringStaging
            : connectionStringProduction;

        // Build URI
        Uri uri = new Uri(new Uri(connectionEnv.ConnectionString), apiPath);
        using (UnityWebRequest genericWebRequest = generateWebRequest(uri))
        {
            // Set timeout
            genericWebRequest.timeout = timeoutSeconds;

            // Add here any headers required (e.g. "Accept" , "Content-Type")
            // Add here any certificate handlers

            float timeBeforeRequest = Time.realtimeSinceStartup;

            // Make the call
            yield return genericWebRequest.SendWebRequest();

            // Is there any error
            // Add here any other accepted HTTP codes (e.g. "System.Net.HttpStatusCode.Created")
            if ((genericWebRequest.isHttpError || genericWebRequest.isNetworkError))
            {
                failAction(genericWebRequest.error + " - " + genericWebRequest.responseCode);
                Debug.LogWarning(genericWebRequest.error);
            }
            else // Success
            {
                if (genericWebRequest.downloadHandler == null || genericWebRequest.downloadHandler.isDone)
                {
                    float timeAfterRequest = Time.realtimeSinceStartup;
                    float latency = timeAfterRequest - timeBeforeRequest;
                    handleSucessResult(genericWebRequest, latency);
                }
                else
                {
                    failAction("downloadHandler is NOT done");
                    Debug.LogWarning(genericWebRequest.error);
                }
            }
        }
    }

    private bool IsStagingEnv
    {
        get
        {
            return networkEnvironment == NetworkEnvironment.Staging;
        }
    }
}
