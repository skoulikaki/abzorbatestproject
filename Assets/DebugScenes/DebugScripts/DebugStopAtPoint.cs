﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugStopAtPoint : MonoBehaviour
{
    public float speed = 1;
    public float stopPoint = 15;
    public float loopAtX = -100;
    public float brakeTrueDistance = 30;

    public bool startStopping = false;

    private bool brakeIsApplied = false;
    private bool awaitBrake = false;
    private float startBrakeDistance;
    private bool readyForBrake;

    private void Update()
    {
        if (brakeIsApplied)
        {
            float trueDist = TrueDistance(stopPoint, transform.localPosition.x);
            Debug.Log(trueDist);

            speed = Mathf.Min(trueDist , Mathf.Abs(speed)) * Mathf.Sign(speed);
        }

        if (awaitBrake)
        {
            if (!readyForBrake)
            {
                if (TrueDistance(stopPoint, transform.localPosition.x) > brakeTrueDistance)
                    readyForBrake = true;
            }

            if (readyForBrake && TrueDistance(stopPoint, transform.localPosition.x) < brakeTrueDistance)
            {
                brakeIsApplied = true;
                readyForBrake = false;
            }
        }

        transform.Translate(speed * Time.deltaTime, 0, 0, Space.Self);

        if (speed < 0 && transform.localPosition.x < loopAtX)
            transform.localPosition = Vector2.zero;
        else if (speed > 0 && transform.localPosition.x > loopAtX)
            transform.localPosition = Vector2.zero;
    }

    private float TrueDistance(float stopPoint, float x)
    {
        float distance = Mathf.Abs(x - stopPoint);
        if (speed < 0)
        {
            if (x < stopPoint)
                distance -= loopAtX;
        }
        else
        {
            if (x > stopPoint)
                distance += loopAtX;
        }

        return distance;
    }

    private void OnValidate()
    {
        if (startStopping)
        {
            startBrakeDistance = TrueDistance(stopPoint, transform.localPosition.x) + loopAtX;
            brakeIsApplied = false;
            readyForBrake = false;
            awaitBrake = true;
            startStopping = false;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(new Vector2(stopPoint, 0), .5f);

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(new Vector2(loopAtX, 0), .5f);

    }
}
