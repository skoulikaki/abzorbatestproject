﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugButtonActions : MonoBehaviour
{
    public RestApiCalls restApiCalls;
    public Text statusText;

    public void DebugButtonGetClicked()
    {
        GetSpinnerValues(
            (spinnerValuesDao, latencyMilliseconds) => statusText.text = string.Format("Service response time {0:0.00}ms with {1} items", latencyMilliseconds, spinnerValuesDao.spinnerValues.Length),
            ShowErrorFromService);
    }

    public void DebugButtonPostClicked()
    {
        PostForSpinnerValue(
            (singleSpinnerValueDao, latencyMilliseconds) => statusText.text = string.Format("Service response time {0:0.00}ms value {1}", latencyMilliseconds, singleSpinnerValueDao.spinnerValue),
            ShowErrorFromService);
    }

    private void PostForSpinnerValue(Action<SingleSpinnerValueDao, float> successAction, Action<string> failAction)
    {
        restApiCalls.PostSingleSpinnerValueFromRest(successAction, failAction);
    }

    private void GetSpinnerValues(Action<SpinnerValuesDao, float> successAction, Action<string> failAction)
    {
        restApiCalls.GetSpinnerValuesFromRest(successAction, failAction);
    }

    private void ShowErrorFromService(string errorDescription)
    {
        statusText.text = string.Format("Error : {0}", errorDescription);
    }
}
