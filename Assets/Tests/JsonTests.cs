﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class JsonTests
    {
        [Test]
        public void JsonSpinnerValuesGetTest()
        {
            string restResponse = "{\"spinnerValues\":[1000,2000,5000,10000,15000,30000,100000,150000,200000]}";

            var spinnerValuesDao = JsonUtility.FromJson<SpinnerValuesDao>(restResponse);

            Assert.NotNull(spinnerValuesDao.spinnerValues);
            Assert.AreEqual(9, spinnerValuesDao.spinnerValues.Length);
        }

        [Test]
        public void JsonSpinnerValuePostTest()
        {
            string restResponse = "{\"spinnerValue\":15000}";

            var singleSpinnerValueDao = JsonUtility.FromJson<SingleSpinnerValueDao>(restResponse);

            Assert.AreEqual(15000, singleSpinnerValueDao.spinnerValue);
        }
    }
}
